<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<div id="reis-cta" >
	<div class="reis-cta-inner">
		<p class="reis-cta-text">
			Je bekijkt de reis:<?php the_title( '<span>', '</span>' ); ?>
		</p>
		<a href="#reis-boeken" class="btn btn-full green">
			Boek direct
		</a>
		
	</div>
</div> 

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="reizen-content-top flex-two-column container-inner-small">
		<div class="reizen-content-top--main-content">
			<div class="content-kader" data-aos="fade-up">
				<h3>Wat ga je doen</h3>
				<?php the_field('reis_omschrijving'); ?>
			</div>
		</div>
		<div class="reizen-content-top--specs" data-aos="fade-up" data-aos-delay="250">
			<?php 
			$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
				if ( $aanbieding ==1){ ?>
				<div class="aanbieding-label">
					<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
				</div>
			<?php } ?>
			<?php the_title( '<h3>', '</h3>' ); ?>
			<div class="star-block">
				<span class="stars">
					<?php 
						if( get_field('reis_review_total') == '1' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '2' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '3' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '4' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '5' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<?php
						}
					?>
				</span>
			</div>
			<div class="reizen-specs--list">
				<ul class="two-column-list">
					<?php echo get_the_term_list( $post->ID, 'reis_soort', '<li class="icon icon-reis-soort">',', ','</li>'); ?>
					<?php echo get_the_term_list( $post->ID, 'niveau_ruiter', '<li class="icon icon-reis-niveau">',', ','</li>'); 
					echo get_the_term_list( $post->ID, 'reis_accommodatie', '<li class="icon icon-reis-accommodatie"> ',', ','</li>'); 
					
					if(get_field('reis_reistijd_per_dag')){
						echo '<li class="icon icon-reis-rijtijd"> ' . get_field('reis_reistijd_per_dag') . '</li>';
					}
					if(get_field('reis_reisdagen')){
						echo '<li class="icon icon-reis-dagen"> ' . get_field('reis_reisdagen') . '</li>';
					}
						echo get_the_term_list( $post->ID, 'reis_overig', '<li class="icon icon-reis-overig">',', ','</li>'); ?>
				</ul>
				<ul class="usp-list">
					<?php
						if( have_rows('pluspunten_van_deze_reis') ):
						    while ( have_rows('pluspunten_van_deze_reis') ) : the_row();
						    	?>
						        <li class="pluspunt-list-item">
						        	<?php the_sub_field('reis_pluspunt'); ?>
						    	</li>
						    	<?php 
						    endwhile;
						else :
						    // no rows found
						endif;
					?>
				</ul>
			</div>
			<div class="reizen-specs--prijs">
				<?php if( have_rows('prijsinformatie') ): ?>
				    <?php while( have_rows('prijsinformatie') ): the_row(); 
				    	?>
				    	Vanaf:<br/>
				    	<?php if( get_sub_field('reis_prijs_old') ): ?>
				    		<span class="old-price">€ <?php the_sub_field('reis_prijs_old'); ?>,-</span>
				    	<?php endif; ?>
				    	<span class="prijs">€ <?php the_sub_field('reis_prijs'); ?>,-</span>
				    	<div class="prijsinfo font-small">
				    		<?php the_sub_field('reis_prijs_toelichting'); ?>
				    	</div>
				    <?php endwhile; ?>
				<?php endif; ?>
			</div>
			<a href="#reis-boeken" class="btn btn-full green">Stel je reis samen</a> <a href="<?php echo esc_url( home_url( '/' ) ); ?>contact" class="btn btn-text black">Stel een vraag</a>
			<div class="social-share">Deel deze reis: </div>
		</div>
	</div>
	<div class="reizen-content-contentbuilder container-inner-small">
		<?php the_content(); ?>
	</div>
	<div class="reis-info-button-block container-inner-small">
		<a href="<?php echo esc_url( get_page_link( 879 ) ); ?>" class="btn btn-full green">Lees onze algemene reisinformatie</a>
	</div>
	<div class="reizen-content-footer" id="reis-boeken">
		
		<div class="reizen-content-footer-left" data-aos="fade-up" data-aos-anchor-trigger="reis-boeken" data-aos-once="true">
			<?php the_title( '<h3>Reis samenstellen: ', '</h3>' ); ?>
			<p>Stel hieronder je reis samen. Je ziet direct een schatting van de kosten. Stuur de informatie naar ons toe en wij maken een vrijblijvende offerte voor je.</p>
			<?php the_field('kies_het_boekingsformulier'); ?>
			<script type="text/javascript">
			jQuery(document).ready(function() {
			  // By Default Disable radio button
			  jQuery(":radio[value=vol]").attr('disabled', true);
			});

		</script>
		<div class="boekingsLinks">
			Lees onze <a href="<?php echo esc_url( get_page_link( 2654 ) ); ?>" target="_blank">Privacy statement</a> en <a href="<?php echo esc_url( get_page_link( 968 ) ); ?>" target="_blank">Algemene Voorwaarden</a>
		</div>
		</div>
		<div class="reizen-content-footer-right" data-aos="fade-up" data-aos-delay="250">
			<h3>Hulp nodig bij het boeken?</h3>
			<p>Het samenstellen van je reis is een belangrijke stap. We snappen dat je hier misschien nog vragen over hebt. Deze beantwoorden we graag!</p>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact" class="btn btn-full green">Contact opnemen</a>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
	<div class="related-reizen">
		<h4>Vergelijkbare reizen</h4>
		<div class="related-reizen-block">
			<?php 
				$related_overruled = get_field('related_overschrijven');
				if ( $related_overruled ==1){ ?>
					<?php
					$post_objects = get_field('related_reizen_items');

					if( $post_objects ): ?>
						<div class="yarpp-related">
							<?php 
								$aos_count = 0;
								foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <?php 
									$image = get_field('hoofdafbeelding');
									$size = 'reis-overview';
									$thumb = $image['sizes'][ $size ];
								?>
							<a href="<?php echo get_permalink();?>">
							<?php 
								$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
									if ( $aanbieding ==1){ ?>
									<div class="aanbieding-label">
										<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
									</div>
								<?php } ?>
							<div 
								class="aanbieding-block-item"
								style="background-image: url('<?php echo esc_url($thumb); ?>');"
								data-aos="fade-up"
								data-aos-delay="<?php echo aos_delay( $aos_count++); ?>"
							>
							
						    	<div class="aanbieding-block-item--content">
						    		<div class="aanbieding-block-item--content-left">
							    		<h3><?php the_title(); ?></h3>
							    		<span><?php the_field('subtitel'); ?></span>
							    	</div>
							    	<div class="aanbieding-block-item--content-right">
							    		<span class="btn btn-full green">Ontdek deze reis</span>
							    	</div>
						    	</div>
						    </div></a>
							<?php endforeach; ?>
						</div>
		    		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; 
				}
				else{ 
					related_entries();
				}
			?>
		</div>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>reizen" class="btn btn-inline-text black right-align">
			Alle bestemmingen
		</a>
	</div>
