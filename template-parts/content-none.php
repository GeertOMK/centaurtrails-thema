<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<section class="no-results not-found">
	
	<div class="page-content"  data-aos="fade-up">>
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

			printf(
				'<p>' . wp_kses(
					/* translators: 1: link to WP admin new post page. */
					__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'centaurtrails' ),
					array(
						'a' => array(
							'href' => array(),
						),
					)
				) . '</p>',
				esc_url( admin_url( 'post-new.php' ) )
			);

		elseif ( is_search() ) :
			?>
			<h2>Helaas, niets gevonden</h2><br/>
			<p>Helaas konden we niet iets vinden over deze zoekterm. Probeer een ander zoekterm of neem contact met ons op.</p>
			
			<form action="<?php echo home_url( '/' ); ?>" method="get">
			    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
			    <input type="submit" alt="Zoeken" value="Zoeken" />
			</form>
			<?php
		else :
			?>

			<p>Helaas konden we niet vinden wat je zocht. Probeer een ander zoekterm of neem contact met ons op.</p>
			<?php
			get_search_form();

		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
