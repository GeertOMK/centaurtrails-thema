<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>
			<div class="article-grid">
				<?php

				/* Start the Loop */
				$aos_count = 0;
				while ( have_posts() ) :
					the_post();

					?>

					<?php 
						$image = get_field('hoofdafbeelding');
						$size = 'site-header';
						$set_image = wp_get_attachment_image_src( $image, $size );
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>
					data-aos="fade-up"
					data-aos-delay="<?php echo aos_delay( $aos_count++); ?>"
					
					>
						<div class="blog-archive-image" <?php if( get_field('hoofdafbeelding') ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>><a href="<?php the_permalink(); ?>"></a> 
						</div>
						<div class="entry-content">
							<h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
							<?php
							the_excerpt( sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers */
									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'centaurtrails' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							) );?>
							<a href="<?php the_permalink(); ?>" class="btn btn-full green">Lees de blog</a> 

						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; ?>
				<article class="nieuwsbrief" data-aos="fade-up" data-aos-delay="<?php echo aos_delay( 1 ); ?>">
					<div class="blog-archive-image"></div>
					<div class="entry-content">
						<?php echo do_shortcode('[gravityform id="104" title="true" description="true" ajax="true"]'); ?>
					</div>
				</article>
			</div> <!-- end article-grid -->
			<?php wp_pagenavi(); 

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
