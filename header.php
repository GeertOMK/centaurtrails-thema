<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Centaur_Trails
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<style>
	/* IE11 */
	@media all and (-ms-high-contrast:none)
	{
	*::-ms-backdrop,
	[data-aos^=fade][data-aos^=fade],[data-aos^=zoom][data-aos^=zoom]{
	opacity: 1;
	}
}
	</style>
	
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'centaurtrails' ); ?></a>
	<div class="topbar">
		<div class="container-inner">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-5',
				'menu_id'        => 'topbar-menu',
			) );
			?>
			<?php 
				echo do_shortcode('[ivory-search id="2943" title="Default Search Form"]');
			?>

		</div>
	</div>

	<?php 
		$image = get_field('hoofdafbeelding');
		$size = 'site-header';
		$set_image = wp_get_attachment_image_src( $image, $size );
	?>
	
	<header id="masthead" class="site-header" <?php if( get_field('hoofdafbeelding') ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>>

		<div class="header-top-content">
			<div class="container-inner">
				<div class="site-branding">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/centaur-trails-logo.png"/ alt="Ga naar home"></a></h1>
				</div><!-- .site-branding -->
				<nav id="site-navigation" class="main-navigation">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
					?>
				</nav>  
			</div>
		</div>
		<div class="content-header-container">
			<div class="container-inner">
				<div class="content-header">
					<?php
					if( is_post_type_archive('reizen') ) :
						?><h1 class="page-title">Onze reizen</h1>
						<span class="subtitle">Welke bestemming kies jij?</span>
						<?php
					elseif ( is_post_type_archive('reviews') ) : ?>
						<h1 class="page-title">Reviews</h1>
						<span class="subtitle">Wat vinden onze reizigers</span>
						<?php
					elseif ( is_singular('reviews') ) : 
						the_title( '<h1 class="entry-title">', '</h1>' );?>
						<span class="subtitle">Reis: <?php
							$post_object = get_field('review_reis');
							if( $post_object ): 
								// override $post
								$post = $post_object;
								setup_postdata( $post ); 
								?>
							    <?php the_title(); ?>
								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						</span>
						<?php
					elseif ( !is_front_page() && is_home() ) : ?>
						<h1 class="page-title">Blog</h1>
						<span class="subtitle">Nieuwsberichten</span>
						<?php
					elseif ( is_front_page()) : ?>
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Logo-centaur-trails-home.png" />
						<span class="subtitle">PAARDRIJVAKANTIES OVER DE HELE WERELD</span>
						<?php
					elseif ( is_archive() ) :
						$term = get_queried_object();?>
						<h1 class="page-title 2"><?php echo single_term_title();?> </h1>
						<?php if( get_field('subtitel', $term) ): ?><span class="subtitle">		<?php the_field('subtitel', $term); ?></span><?php endif; ?>
						<?php
					elseif ( is_search() ) : ?>
						<h1 class="page-title">
							<?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Alles over: %s', 'centaurtrails' ), '<span>' . get_search_query() . '</span>' );
							?>
						</h1>
						<?php
					else :
						the_title( '<h1 class="entry-title">', '</h1>' );?>
						<?php if( get_field('subtitel') ): ?><span class="subtitle"><?php the_field('subtitel'); ?></span><?php endif; ?>
						<?php
					endif;	?>	

				</div>
			</div>
		</div>
	</header><!-- #masthead -->
	
	<div id="content" class="site-content">
		<div class="container-inner">
			<div class="header-top-submenu">
				<div class="container-inner">
					<nav id="site-navigation-secondary" class="main-navigation-second">
						<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sub-menu") ) : ?>
						<?php endif;?>

					</nav><!-- #site-navigation -->
				</div>
			</div>