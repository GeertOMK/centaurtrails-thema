<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
	
	<h4 class="filterresult">We hebben <?php echo $query->found_posts; ?> reizen voor je:</h4>
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
		<div class="reizen-resultaten"> 
			<?php get_template_part( 'template-parts/content-reizen', 'detail' ); ?>
		</div>
		<?php	}
	?>
	
	<?php
}
else
{
	echo "<div class=\"filter_no_result\"><h3>Helaas, we hebben geen reizen gevonden</h3>We hebben nog geen reizen die voldoen aan je zoekfilter. Het kan zijn dat we al wel contact hebben met partners voor de reis die je zoekt, maar dat dit aanbod nog niet op de website staat. Neem daarom gerust contact met ons op en we kijken met je mee naar de mogelijkheden of eventuele interessante alternatieve reizen.<br/><br/><a href=\"/contact\" class=\"btn btn-full orange\">Neem contact op</a></div>";
	get_template_part( 'template-parts/reizen-populair' );
}
?>