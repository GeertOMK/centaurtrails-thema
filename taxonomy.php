<?php
/**
 * The main template file fot the taxonomy pages
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

get_header();
?>
	<?php $term_desc = term_description($term_id, 'reizen');
		if ( '' !== $term_desc )
		{
		    echo '<div class="tax-description">' .$term_desc. '</div>';
		}?>
		<span id="open-filter-button" class="btn btn-full green">Reizen filteren</span>
	<div class="page-content">
		<div id="secondary" data-aos="fade-up">
			<div class="filtersystem">
				<?php echo do_shortcode('[searchandfilter slug="filter-je-reis"]'); ?>
			</div>
			  
			<span id="close-filter-x" class="btn btn-full green">&cross;</span>
			<span id="close-filter-button" class="btn btn-full green">Filter toepassen</span>
		</div>
		<div id="primary" class="content-area" data-aos="fade-up" data-aos-delay="250">
			<main id="main" class="site-main">
				<?php echo do_shortcode('[searchandfilter id="1867" show="results"]'); ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #pagecontent -->
<?php
get_footer();