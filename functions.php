<?php
/**
 * Centaur Trails functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Centaur_Trails
 */

if ( ! function_exists( 'centaurtrails_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function centaurtrails_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Centaur Trails, use a find and replace
		 * to change 'centaurtrails' to the name of your theme in all the template files.
		 */

		load_theme_textdomain( 'centaurtrails', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'centaurtrails' ),
			'menu-2' => esc_html__( 'Disclaimer', 'centaurtrails' ),
			'menu-3' => esc_html__( 'Footer continent', 'centaurtrails' ),
			'menu-4' => esc_html__( 'Footer soort reis', 'centaurtrails' ),	
			'menu-5' => esc_html__( 'Topbar', 'centaurtrails' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'centaurtrails_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );


		add_theme_support( 'editor-styles' );
		add_editor_style( 'style-editor.css' );
	}
endif;
add_action( 'after_setup_theme', 'centaurtrails_setup' );


// add some custom thumbnails
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
	function wpdocs_theme_setup() {
	    add_image_size( 'site-header', 1600 ); // 1600 pixels wide (and unlimited height)
	    add_image_size( 'reis-overview', 600 ); // 600 pixels wide (and unlimited height)
	    add_image_size( 'continent-overview', 900); // 700 pixels wide 700 height, center gesneden)
	}



// remove dashicons in frontend to non-admin 
    function wpdocs_dequeue_dashicon() {
        if (current_user_can( 'update_core' )) {
            return;
        }
        wp_deregister_style('dashicons');
    }
    add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function centaurtrails_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'centaurtrails_content_width', 640 );
}
add_action( 'after_setup_theme', 'centaurtrails_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function centaurtrails_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'centaurtrails' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'centaurtrails' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar(array(
	    'name' => 'sub-menu',
	    'before_widget' => '<div class = "widgetizedArea">',
	    'after_widget' => '</div>',
	    'before_title' => '<h3>',
	    'after_title' => '</h3>',
	  )
	);
}
add_action( 'widgets_init', 'centaurtrails_widgets_init' );

/**
 * ACF Blocks
 */
function register_acf_block_types() {

    // register a get post block
    acf_register_block_type(array(
        'name'              => 'centaur-post',
        'title'             => __('Centaur gerelateerd'),
        'description'       => __('Compion gerelateerd'),
        'render_template'   => 'template-parts/content-block-related.php',
        'category'          => 'centaur',
        'icon'              => 'admin-post',
        'keywords'          => array( 'related', 'post', 'case', 'artikel', 'bericht', 'centaur' ),
    ));
}
// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}

/**
 * Enqueue scripts and styles.
 */
function centaurtrails_scripts() {
	wp_enqueue_style( 'centaurtrails-style', get_stylesheet_uri(), array(), date('U') );

	wp_enqueue_script( 'animate-on-scroll-js', get_template_directory_uri() . '/js/aos.js', array(), '20151215', true );

	wp_enqueue_script( 'centaurtrails-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'centaurtrails-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'centaurtrails_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 *
 * Color palette Gutenberg
 *
 */
// Adds support for editor color palette.
function mytheme_setup_theme_supported_features() {
    add_theme_support( 'editor-color-palette', array(
        array(
            'name' => __( 'Centaur Black', 'themeLangDomain' ),
            'slug' => 'centaur-black',
            'color' => '#363636',
        ),
        array(
            'name' => __( 'Centaur Orange', 'themeLangDomain' ),
            'slug' => 'centaur-orange',
            'color' => '#df7021',
        ),
        array(
            'name' => __( 'Centaur Green', 'themeLangDomain' ),
            'slug' => 'centaur-green',
            'color' => '#7bb534',
        ),
        array(
            'name' => __( 'Centaur Light Green', 'themeLangDomain' ),
            'slug' => 'centaur-licht-green',
            'color' => '#e3e3d3',
        ),
        array(
            'name' => __( 'Centaur White', 'themeLangDomain' ),
            'slug' => 'centaur-wit',
            'color' => '#ffffff',
        ),
    ) );
}

add_action( 'after_setup_theme', 'mytheme_setup_theme_supported_features' );

/**
 * Disable the custom color picker.
 */
function tabor_gutenberg_disable_custom_colors() {
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'tabor_gutenberg_disable_custom_colors' );


function acf_set_featured_image( $value, $post_id, $field  ){
    
    if($value != ''){
	    //Add the value which is the image ID to the _thumbnail_id meta data for the current post
	    add_post_meta($post_id, '_thumbnail_id', $value);
    }
 
    return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=hoofdafbeelding', 'acf_set_featured_image', 10, 3);

/**
 * Gutenberg scripts and styles
 * @link https://www.billerickson.net/block-styles-in-gutenberg/
 */
function be_gutenberg_scripts() {

	wp_enqueue_script(
		'be-editor', 
		get_stylesheet_directory_uri() . '/js/block-styles.js', 
		array( 'wp-blocks', 'wp-dom' ), 
		filemtime( get_stylesheet_directory() . '/js/block-styles.js' ),
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'be_gutenberg_scripts' );


/**
 * Enqueue block editor style
 */
function legit_block_editor_styles() {
    wp_enqueue_style( 'legit-editor-styles', get_theme_file_uri( '/sass/backend/style-editor.css' ), false, '1.0', 'all' );
}
add_action( 'enqueue_block_editor_assets', 'legit_block_editor_styles' );


/**
 * Add ACF options page
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Algemene instellingen',
		'menu_title'	=> 'Algemeen',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

/**
 * Graivty Wiz // Gravity Forms // Move Currency Symbol from the Right to the Left (i.e. "0,00 €" to "€ 0,00")
 * http://gravitywiz.com/how-do-i-move-the-currency-symbol-from-the-right-to-the-left-for-gravity-forms/
 */
add_filter( 'gform_currencies', 'gw_modify_currencies' );
function gw_modify_currencies( $currencies ) {

	$currencies['EUR'] = array(
		'name'               => esc_html__( 'Euro', 'gravityforms' ),
		'symbol_left'        => '&#8364;',
		'symbol_right'       => '',
		'symbol_padding'     => ' ',
		'thousand_separator' => '.',
		'decimal_separator'  => ',',
		'decimals'           => 2
	);

	return $currencies;
}

//* Review block *//
add_action('acf/init', 'review_block');
function review_block() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'review',
			'title'				=> __('Centaur Review'),
			'description'		=> __('Een block met reviews.'),
			'render_callback'	=> 'review_block_render_callback',
			'category'			=> 'centaur',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'review', 'beoordeling' ),
		));
	}
}
function review_block_render_callback( $block, $content = '', $is_preview = false ) {
	if( file_exists( get_theme_file_path("/template-parts/content-block-review.php") ) ) {
		include( get_theme_file_path("/template-parts/content-block-review.php") );
	}
}


function aos_delay( $aos_count, $delay = 100 ) {
	return $aos_count * $delay;
}