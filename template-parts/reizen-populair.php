<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<section class="reis-gerelateerd">
	<h4>Populaire reizen</h4>
	<div class="reis-gerelateerd-block">
	<?php
		$post_objects = get_field('populaire_reizen', 'option');

		if( $post_objects ):
			$aos_count = 0;
		?>
		
		    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
		        <?php setup_postdata($post); ?>
		        <?php 
					$image = get_field('hoofdafbeelding');
					$size = 'reis-overview';
					$set_image = wp_get_attachment_image_src( $image, $size );
				?>
				<div
					class="reis-gerelateerd-block-wrap"
					data-aos="fade-up"
					data-aos-delay="<?php echo aos_delay( $aos_count++); ?>">

					<div class="reis-gerelateerd-block-item" style="background-image: url('<?php echo $set_image['0'];?>');">
		            <a href="<?php the_permalink(); ?>"  class="reis-gerelateerd-block-item--link">
		            	<div class="bottom-gradient"></div>
		            	<span class="reis-gerelateerd-block-item--title"><?php the_title(''); ?></span>
		            </a>
		        </div>

				</div>

		    <?php endforeach; ?>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif;
	?>
	</div>
</section>