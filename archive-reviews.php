<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>
			<div class="article-grid">
				<?php
				$aos_count = 0;
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					?>

					<?php 
						$image = get_field('hoofdafbeelding');
						$size = 'site-header';
						$set_image = wp_get_attachment_image_src( $image, $size );
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> 
						data-aos="fade-up"
						data-aos-delay="<?php echo aos_delay( $aos_count++); ?>"
					>
						<div class="blog-archive-image" <?php if( get_field('hoofdafbeelding') ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>><a href="<?php the_permalink(); ?>"></a> 
						</div>
						<div class="entry-content">
							<h3><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h3>
							<span class="stars">
								<?php 
									if( get_field('aantal_hoefijzers') == '1' ) {
										?>
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<?php
									}
									else if( get_field('aantal_hoefijzers') == '2' ) {
										?>
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<?php
									}
									else if( get_field('aantal_hoefijzers') == '3' ) {
										?>
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<?php
									}
									else if( get_field('aantal_hoefijzers') == '4' ) {
										?>
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
										<?php
									}
									else if( get_field('aantal_hoefijzers') == '5' ) {
										?>
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
										<?php
									}
								?>
							</span>
							<span class="review-meta">
								<i><?php the_field('referentie_naam'); ?> (<?php the_field('referentie_leeftijd'); ?>) beoordeelde de reis: <?php
								$post_object = get_field('review_reis');

								if( $post_object ): 
									// override $post
									$post = $post_object;
									setup_postdata( $post ); 
									?>
								    	<?php the_title(); ?>
								    <?php wp_reset_postdata(); ?>
								<?php endif; ?></i>
							</span>
							<?php
							the_excerpt( sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers */
									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'centaurtrails' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							) );?>
							<a href="<?php the_permalink(); ?>" class="btn btn-full green">Lees de review</a> 

						</div><!-- .entry-content -->
					</article><!-- #post-<?php the_ID(); ?> -->

				<?php endwhile; ?>
			</div> <!-- end article-grid -->
			<?php wp_pagenavi(); 

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
