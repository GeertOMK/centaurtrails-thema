<?php

/**
 * Client Block Template.
 *
 * @param   array $block The block data including all properties and settings.
 * @param   bool $is_preview True when editing in the back-end.
 * @param   int $post_id The post being edited.
 */

$post_objects = get_field('related_posts');
$colum_use = get_field('gebruik_je_dit_in_een_nieuwe_rij');
?>

<div class="relatedBlock <?php if ( $colum_use ==1){ echo 'row-related';} ?>">
        <?php

        if( $post_objects ): ?>
            <?php foreach( $post_objects as $post_object): ?>
                 <?php setup_postdata($post); ?>
                 <?php 
                    $image = get_field('hoofdafbeelding', $post_object->ID);
                    $size = 'reis-overview';
                    $set_image = wp_get_attachment_image_src( $image, $size );
                ?>
                <article id="post--<?php the_ID( $post_object->ID ); ?>" <?php post_class(); ?>>
                    <div class="blog-archive-image" <?php if( get_field('hoofdafbeelding', $post_object->ID) ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>>
                    </div>
                    <div class="entry-content">
                        <h3><?php echo get_the_title($post_object->ID); ?></h3>
                        <a href="<?php echo esc_url( get_permalink( $post_object->ID) ); ?>" class="btn btn-full green">Lees de blog</a> 

                    </div><!-- .entry-content -->
                </article><!-- #post-<?php the_ID(); ?> -->

            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
</div>