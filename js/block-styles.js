/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */
wp.domReady( () => {
	wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'fill' );
	wp.blocks.registerBlockStyle(
		'core/button',
		[
			{
				name: 'groen',
				label: 'groen',
				isDefault: true,
			},
			{
				name: 'oranje',
				label: 'Oranje',
			},
			{
				name: 'tekst',
				label: 'Tekst',
			}
		]
	);
} );