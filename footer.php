<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Centaur_Trails
 */

?>
		</div><!-- .container-inner -->
	</div><!-- #content -->

	<!-- load insta for specific pages -->
	<?php
		if ( is_front_page() || is_page(2139) ) :
			?>
			<?php get_template_part( 'template-parts/content-socialfeed' ); ?>
			<?php
		endif;	?>	
	<!-- end load insta for specific pages -->

	<div class="call-to-action" id="cta">
		<div class="container-inner">
			<?php
			$term = get_queried_object();
			if( get_field('normale_call_to_action_overschrijven', $term) == 1 ) {
				?>
			    <h2><?php the_field('cta_title', $term); ?></h2>
				<p><?php the_field('cta_tekst', $term); ?></p>
				<?php $link = get_field('cta_link', $term); ?>
				<a href="<?php echo esc_url( $link ); ?>" class="btn btn-full green"><?php the_field('cta_link_tekst', $term); ?></a> 
				<?php
			}
			else{
				?>
				<h2>Wat is jouw droomreis?</h2>
				<p>Er zijn veel verschillende paardrijvakanties, kijk snel welke het beste bij jou past!</p>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>reizen" class="btn btn-full green">Ontdek onze bestemmingen</a> 
				<?php
			}

			?>
		</div>
	</div>
	
	<footer class="site-footer">
		<div class="site-footer-contact">
			<div class="container-inner">
				<div class="footer-block">
					<div class="footer-block-item footer-one">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/centaur-trails-logo.png" alt="Ga naar home" /><br/>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-2',
							'menu_id'        => 'footer-menu',
						) );
						?>
						© Centaur Trails<br/>
						<br/>
						<h3>Blijf op de hoogte</h3>
						<p>Schrijf je in voor onze nieuwsbrief</p>
						<?php echo do_shortcode('[gravityform id="108" title="false" description="false" ajax="true"]'); ?>
						
					</div>
					<div class="footer-block-item footer-two">
						<h4>Reizigersinformatie</h4>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-3',
							'menu_id'        => 'footer-menu',
						) );
						?>
					</div>
					<div class="footer-block-item footer-three">
						<h4>Soorten reizen</h4>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-4',
							'menu_id'        => 'footer-menu',
						) );
						?>
					</div>
					<div class="footer-block-item footer-four">
						<h4>Contact</h4>
						Centaur Trails<br/>
						<a href="mailto:info@centaurtrails.com">info@centaurtrails.com</a><br/>
						Tel.nr <a href="tel:+31629456787">+31 (0)6 29 45 67 87</a><br/>
						KvK nr 71648763<br/>
						<br/>
						<a class="social-icon" href="https://www.instagram.com/centaurtrailspaardrijvakanties/" target="_blank"><i class="fab fa-instagram"></i> Instagram</a> <br/>
						<a class="social-icon" href="https://www.facebook.com/CentaurTrailsPaardrijvakanties/" target="_blank"><i class="fab fa-facebook-f"></i> Facebook</a><br/>
						<a class="social-icon" href="https://www.youtube.com/channel/UC9qJqmQFk3dUQOY9-ItmznA/" target="_blank"><i class="fab fa-youtube"></i> YouTube</a>
						<br/>
						<img class="keurmerk" src="<?php echo get_stylesheet_directory_uri(); ?>/images/ggto-logo.png" />
						<br/>
						© Centaur Trails<br/>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
 <script type="text/javascript">
    jQuery(function() {
  jQuery('.reizen-content-top--specs a[href*=#]').on('click', function(e) {
    e.preventDefault();
    jQuery('html, body').animate({ scrollTop: jQuery(jQuery(this).attr('href')).offset().top}, 500, 'linear');
  });
}); 
</script>
<?php wp_footer(); ?>

</body>
</html>