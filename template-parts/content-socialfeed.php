<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<section class="instagram container-inner" data-aos="fade-up">
	<h4><i class="fab fa-instagram"></i> Hier zijn we nu</h4>
	<?php echo do_shortcode('[instagram-feed]'); ?>
</section>