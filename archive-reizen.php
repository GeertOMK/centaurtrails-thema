<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

get_header();
?>
	<?php $term_desc = term_description($term_id, 'reizen');
		if ( '' !== $term_desc )
		{
		    echo '<div class="tax-description">' .$term_desc. '</div>';
		}?>
    	<span id="open-filter-button" class="btn btn-full green">Reizen filteren</span>
	<div class="page-content">
		<div id="secondary" data-aos="fade-up">
			<div class="filtersystem">
				<?php echo do_shortcode('[searchandfilter slug="filter-je-reis"]'); ?>
			</div>
			  
			<span id="close-filter-x" class="btn btn-full green">&cross;</span>
			<span id="close-filter-button" class="btn btn-full green">Filter toepassen</span>
		</div>
		<div id="primary" class="content-area" data-aos="fade-up" data-aos-delay="250">
			<main id="main" class="site-main">
				<?php echo do_shortcode('[searchandfilter id="1867" show="results"]'); ?>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div><!-- #pagecontent -->
<?php
get_footer();
 