<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<?php 
	$image = get_field('hoofdafbeelding');
	$size = 'site-header';
	$set_image = wp_get_attachment_image_src( $image, $size );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="blog-archive-image" <?php if( get_field('hoofdafbeelding') ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>><a href="<?php the_permalink(); ?>"></a> 
	</div>
	<div class="entry-content">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php
		the_excerpt( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'centaurtrails' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );?>
		<a href="<?php the_permalink(); ?>" class="btn btn-full green">Lees verder</a> 

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
