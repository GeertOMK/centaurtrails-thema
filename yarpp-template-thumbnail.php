<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>
<?php if (have_posts()):
		$aos_count = 0;
	?>
	<?php while (have_posts()) : the_post(); ?>
		<?php
		$image = get_field('hoofdafbeelding');
		$size = 'reis-overview';
		$set_image = wp_get_attachment_image_src( $image, $size );
		?>
		<a 
			class="aanbieding-link" href="<?php the_permalink() ?>" 
			data-aos="fade-up"
			data-aos-delay="<?php echo aos_delay( $aos_count++); ?>"
		>
		<?php 
			$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
				if ( $aanbieding ==1){ ?>
				<div class="aanbieding-label">
					<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
				</div>
			<?php } ?>
		<div class="aanbieding-block-item" style="background-image: url('<?php echo $set_image['0'];?>');">
			
	    	<div class="aanbieding-block-item--content">
	    		<div class="aanbieding-block-item--content-left">
		    		<h3><?php the_title(); ?></h3>
		    		<span><?php the_field('subtitel'); ?></span>
		    	</div>
		    	<div class="aanbieding-block-item--content-right">
		    		<span class="btn btn-full green">Ontdek deze reis</span>
		    	</div>
	    	</div>
	    </div></a>
	    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>


	<?php endwhile; ?>
<?php endif; ?>
