<?php
/*
    REVIEW BLOCK
    todo: inline style voor .hoefijzers en img naar scss 
*/
$reviews = get_field('review');


?>

<div class="block-review">

        <?php
        if( $reviews ): ?>
            <?php foreach( $reviews as $review): ?>
                <?php
                    // Post
                    $id             = $review->ID;
                    $title          = get_the_title( $id );
                    $link           = esc_url( get_permalink(  $id ) );
                    $hoefijzer_img  = get_stylesheet_directory_uri() ."/images/icons/hoefijzer-orange.svg";

                    // ACF fields
                    $beoordeling    = intval( get_field('aantal_hoefijzers', $id  ) );
                    $ref_naam       = get_field('referentie_naam', $id );
                    $ref_leeftijd   = get_field('referentie_leeftijd', $id ); 
                ?>

                <article id="post--<?php echo $id ?>" <?php post_class(); ?>>
                        <span class="review_title">"<?php echo $title; ?>"
                        </span>
                        <span class="review_meta">
                            <strong><?php echo $ref_naam; ?></strong> 
                            (<?php echo $ref_leeftijd; ?>) beoordeelde deze reis met: 
                        </span>
                        <div class="hoefijzers" style="margin-bottom:15px;">
                             <?php for( $hoefijzer = 1; $hoefijzer <= $beoordeling; $hoefijzer++): ?>
                                <img class="review-icon" src="<?php echo $hoefijzer_img;?>" style="width:23px;margin-right:3px"/>
                             <?php endfor; ?>
                        </div>

                        <a href="<?php echo $link; ?>" class="btn btn-full green">Lees deze beoordeling</a> 
                </article>

            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
        
        <?php
         if ( !$review && $is_preview ) echo "Geen reviews geselecteerd voor dit blok.";
        ?>

</div>