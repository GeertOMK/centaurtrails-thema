<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 Template Name: Contactpagina
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<div class="contact-block">
			<div class="contact-block-left" data-aos="fade-up">
				<?php echo do_shortcode('[gravityform id="3" title="true" description="false" ajax="true"]'); ?>
			</div>
			<div class="contact-block-right" data-aos="fade-up" data-aos-delay="250">
				<h4>Centaur Trails</h4>
				<p>We hebben een groot aanbod paardrijvakanties over de hele wereld. Staat je droomreis er niet tussen? Neem contact op en wij helpen je met een reis op maat!</p>
				<strong>Centaur Trails Paardrijvakanties</strong><br/>
				<a class="btn btn-inline-text green" href="mailto:info@centaurtrails.nl"><strong>info@centaurtrails.nl</strong></a><br/>
				<a class="btn btn-inline-text green" href="tel:31629456787"><strong>Tel.nr +31 (0)6 29 45 67 87</strong></a><br/>
				KvK nr 71648763<br/>
				<br/>
				<div class="callback-form" data-aos="fade-up">
					<?php echo do_shortcode('[gravityform id="4" title="true" description="false" ajax="true"]'); ?>
				</div>
			</div>
		</div>
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
