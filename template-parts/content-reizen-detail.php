<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>
<?php 
	$image = get_field('hoofdafbeelding');
	$size = 'reis-overview';
	$set_image = wp_get_attachment_image_src( $image, $size );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="reizen-archive-top flex-two-column">
		<div class="reizen-archive-image" <?php if( get_field('hoofdafbeelding') ): ?>style="background-image: url('<?php echo $set_image['0'];?>');"<?php endif; ?>>
			<a href="<?php echo get_permalink();?>"></a>
		</div>
		<div class="reizen-archive-excerpt">
			<?php 
			$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
				if ( $aanbieding ==1){ ?>
				<div class="aanbieding-label">
					<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
				</div>
			<?php } ?>
			<?php 
			the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
			<div class="star-block">
				<span class="stars">
					<?php 
						if( get_field('reis_review_total') == '1' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '2' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '3' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '4' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
							<?php
						}
						else if( get_field('reis_review_total') == '5' ) {
							?>
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
							<?php
						}
					?>
				</span>
			</div>
			<div class="reis-excerpt"><?php the_field('reis_korte_omschrijving'); ?></div>
			<ul class="two-column-list">
				<?php
					if( have_rows('pluspunten_van_deze_reis') ):
					    while ( have_rows('pluspunten_van_deze_reis') ) : the_row();
					    	$show_pluspunt_true = get_sub_field('reis_pluspunt_tonen');

					    	if ( $show_pluspunt_true ==1){
							?>
					        <li class="pluspunt-list-item"> <?php the_sub_field('reis_pluspunt'); ?>
					    	</li>
	
					    	<?php
					    	} 
					    endwhile;
					else :
					    // no rows found
					endif;
				?>
			</ul>
		</div>
	</div>
	<div class="reizen-archive-footer">
		<div class="reizen-archive-footer--specs">
			<ul class="">
				<?php echo get_the_term_list( $post->ID, 'reis_soort', '<li class="icon icon-reis-soort">',', ','</li>'); ?><?php echo get_the_term_list( $post->ID, 'niveau_ruiter', '<li class="icon icon-reis-niveau">',', ','</li>'); ?><?php
				if(get_field('reis_reistijd_per_dag')){
					echo '<li class="icon icon-reis-rijtijd"> ' . get_field('reis_reistijd_per_dag') . '</li>';
				}
				echo get_the_term_list( $post->ID, 'reis_accommodatie', '<li class="icon icon-reis-accommodatie"> ',', ','</li>'); 
				if(get_field('reis_reisdagen')){
					echo '<li class="icon icon-reis-dagen"> ' . get_field('reis_reisdagen') . '</li>';
				}
				echo get_the_term_list( $post->ID, 'reis_overig', '<li class="icon icon-reis-overig">',', ','</li>'); ?>
			</ul>
		</div>
		<div class="reizen-archive-footer--button">
			<?php if( have_rows('prijsinformatie') ): ?>
			    <?php while( have_rows('prijsinformatie') ): the_row(); 
			    	?>
			    	<div class="prijs-block">
				    	Vanaf: <span class="prijs">€ <?php the_sub_field('reis_prijs'); ?>,-</span>
				    </div>
			    <?php endwhile; ?>
			<?php endif; ?>
			<div class="reis-button">
				<a href="<?php echo get_permalink();?>" class="btn btn-full green">Ontdek deze reis</a>
			</div>
		</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
