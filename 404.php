<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Centaur_Trails
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<center>
					<div class="page-content">
						<h2>Helaas, deze pagina bestaat niet (meer)</h2><br/>
						<p>Helaas konden we deze pagina niet vinden. Zoek hieronder naar de info die je zocht of neem contact met ons op.</p>
						
						<form action="<?php echo home_url( '/' ); ?>" method="get">
						    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
						    <input type="submit" alt="Zoeken" value="Zoeken" />
						</form>
					</div><!-- .page-content -->
				</center>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
