<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>  data-aos="fade-up">>
	<div class="star-block">
		<span class="stars">
			<?php 
				if( get_field('aantal_hoefijzers') == '1' ) {
					?>
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<?php
				}
				else if( get_field('aantal_hoefijzers') == '2' ) {
					?>
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<?php
				}
				else if( get_field('aantal_hoefijzers') == '3' ) {
					?>
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<?php
				}
				else if( get_field('aantal_hoefijzers') == '4' ) {
					?>
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-green.svg" />
					<?php
				}
				else if( get_field('aantal_hoefijzers') == '5' ) {
					?>
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<img class="review-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hoefijzer-orange.svg" />
					<?php
				}
			?>
		</span>
	</div>
	<div class="entry-content">
		<?php
		the_content();

		?>
		<br/>
		<span class="review-meta">
			<i><?php the_field('referentie_naam'); ?> (<?php the_field('referentie_leeftijd'); ?>) schreef deze beoordeling. <?php the_field('referentie_naam'); ?> reisde: <?php the_field('reis_formatie'); ?></i>
		</span>
	</div><!-- .entry-content -->
	<div class="related-reviews">
		<h4>Overtuigd? Boek deze reis:</h4>
		<?php
		$post_object = get_field('review_reis');
		if( $post_object ): 
			// override $post
			$post = $post_object;
			setup_postdata( $post ); 
			?>
		    	<div class="related-reviews"> 
					<?php get_template_part( 'template-parts/content-reizen', 'detail' ); ?>
				</div>

		<?php wp_reset_postdata(); ?>
		<?php endif; ?>
	</div>
	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'centaurtrails' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
