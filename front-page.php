<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Centaur_Trails
 */

get_header();
?>
<section 
	class="fp-search"
	data-aos="fade-up"
>
	<?php echo do_shortcode('[searchandfilter id="2053"]'); ?>
</section>
<section class="fp-usp">
	<?php
		if( have_rows('usps_van_centaur') ):

			$aos_count = 1;

			while ( have_rows('usps_van_centaur') ) : the_row();
		
		    	?>
				<div
					class="fp-usp-item" 
					data-aos="fade-up"
					data-aos-delay="<?php echo aos_delay( $aos_count++, 400); ?>" 
				>
		    		<img src="<?php the_sub_field('usp_icoon'); ?>" />
		    		<div class="fp-usp-item-content">
			    		<span class="fp-usp-item-title"><?php the_sub_field('usp_titel'); ?></span>
				        <?php the_sub_field('usp_subtekst');
				        ?>
				    </div>
			    </div>
				<?php
				
		    endwhile;
		else :

		endif;
	?>
</section>
<section class="fp-aanbiedingen flex-two-column">
	<div class="aanbieding-block"  data-aos="fade-up">
		<h4>Aanbiedingen</h4>
		<?php
		$post_object = get_field('reis_aanbieding');
		if( $post_object ): 

			// override $post
			$post = $post_object;
			setup_postdata( $post ); 

			$image = get_field('hoofdafbeelding');
			$size = 'continent-overview';
			$set_image = wp_get_attachment_image_src( $image, $size );
			?>
			<a class="aanbieding-link" href="<?php echo get_permalink();?>">
			<?php 
				$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
					if ( $aanbieding ==1){ ?>
					<div class="aanbieding-label">
						<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
					</div>
				<?php } ?>
			<div class="aanbieding-block-item" style="background-image: url('<?php echo $set_image['0'];?>');">

		    	<div class="aanbieding-block-item--content">
		    		<div class="aanbieding-block-item--content-left">
			    		<h3><?php the_title(); ?></h3>
			    		<span><?php the_field('subtitel'); ?></span>
			    	</div>
			    	<div class="aanbieding-block-item--content-right">
			    		<span class="btn btn-full green">Ontdek deze reis</span>
			    	</div>
		    	</div>
		    </div></a>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>aanbieding/aanbiedingen/" class="btn btn-inline-text black right-align">
			Alle aanbiedingen
		</a>
	</div>
	<div class="aanbieding-block"  data-aos="fade-up" data-aos-delay="300" >
		<h4>Uitgelicht</h4>
		<?php
		$post_object = get_field('reis_uitgelicht');
		if( $post_object ): 

			// override $post
			$post = $post_object;
			setup_postdata( $post ); 

			$image = get_field('hoofdafbeelding');
			$size = 'continent-overview';
			$set_image = wp_get_attachment_image_src( $image, $size );
			?>
			<a class="aanbieding-link" href="<?php echo get_permalink();?>">
			<?php 
				$aanbieding = get_field('is_deze_reis_uitgelicht_of_een_aanbieding');
					if ( $aanbieding ==1){ ?>
					<div class="aanbieding-label">
						<?php the_field('kies_de_actietekst_voor_deze_reis'); ?>
					</div>
				<?php } ?>
			<div class="aanbieding-block-item" style="background-image: url('<?php echo $set_image['0'];?>');">
	
		    	<div class="aanbieding-block-item--content">
		    		<div class="aanbieding-block-item--content-left">
			    		<h3><?php the_title(); ?></h3>
			    		<span><?php the_field('subtitel'); ?></span>
			    	</div>
			    	<div class="aanbieding-block-item--content-right">
			    		<span class="btn btn-full green">Ontdek deze reis</span>
			    	</div>
		    	</div>
		    </div></a>
		    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		<?php endif; ?>
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>aanbieding/uitgelicht/" class="btn btn-inline-text black right-align">
			Alle uitgelichte reizen
		</a>
	</div>

</section>

<?php get_template_part( 'template-parts/reizen-populair' ); ?>

<section class="fp-discover-world">
	<div data-aos="fade-up">
	<h2>Ontdek de wereld</h2>
	<p class="intro">Van ranches in de Verenigde Staten en safari’s in Botswana tot klassieke dressuur in Portugal en trektochten door Mongolië: waar gaat jouw volgende reis naar toe?</p>
	</div>
	<?php if( have_rows('continenten-overzicht') ): ?>
		<div class="continents">
			<?php 
				$aos_count = 0;
				while( have_rows('continenten-overzicht') ): the_row(); 

				// vars
				$image = get_sub_field('continent_foto');
    			$url = $image['url'];
				$size = 'continent-overview';
   				$thumb = $image['sizes'][ $size ];

				$name = get_sub_field('continent_naam');
				$content = get_sub_field('continent_tekst');
				$link = get_sub_field('continent_link');

				?>

				
					<div
						class="continents-item-wrap"
						data-aos="fade-up"
						data-aos-delay="<?php echo aos_delay( $aos_count++); ?>"
					>
						<div class="continents-item" style="background-image: url('<?php echo esc_url($thumb); ?>')"
					
						
						>
							<a class="continents-item--link" href="<?php echo $link; ?>">
								<div class="continents-item--hover">
									<span class="continent-item--hover--title"><?php echo $name; ?></span>
									<p><?php echo $content; ?></p>
									<span class="btn btn-full orange">Ontdek onze reizen</span>
								</div>
								<div class="bottom-gradient"></div>
								<span class="continents-item--title"><?php echo $name; ?></span>
							</a>
						</div>
					</div>

			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</section>
<section class="fp-about-centaur flex-two-column" data-aos="fade-up">
	<?php if( have_rows('info_over_centaur') ): ?>
		<?php while( have_rows('info_over_centaur') ): the_row(); ?>
		<div>
			<h2><?php the_sub_field('over_centaur_titel'); ?></h2>
			<p><?php the_sub_field('over_centaur_tekst'); ?></p>
			<a class="btn btn-full green" href="<?php the_sub_field('over_centaur_link'); ?>">Lees meer over ons</a>
		</div>
		<div>
			<?php 
			$image = get_sub_field('over_centaur_foto');
			if( !empty( $image ) ): ?>
			    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			<?php endif; ?>
		</div>					
	    <?php endwhile; ?>
	<?php endif; ?>	
</section>

<?php
get_footer();
